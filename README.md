# WebAmpel

**Hello world**,
dieses kleine Javaskript umrandet im Web Websiten anhand von Ampelfarben.
Das Programm ist opensource und die Listen von guten Vorbildern (grün)
und von vermeidbaren Seiten (orange) kann laufend ergänzt werden.
Seiten, die noch nicht kategorisiert sind erscheinen grau.
Eine kleine Hilfestellung für unseren Weg nach Utopia.
*(Programmiert von jen_mary_kay)/*

##installation
1. gehe in firefox in der URL Leiste auf "about:debugging"
2. dann auf "dieser firefox"
3. gehe auf "temporäres addon laden"
4. lade dort die datei "manifest.json" hoch
5. dann kannst du in jedem beliebigen URL eingeben und erhälst einen farbigen Rand
6. viel Spaß mit unserer Webampel!
7. Wir freuen uns über Feedack ('#Utopia_Webampel')
